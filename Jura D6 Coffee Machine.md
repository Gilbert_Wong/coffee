Jura D6 Coffee Machine

左轮旋转

Cappuccino、Kaffe、Espresso

旋转至想喝的咖啡，按压左轮，咖啡机开始制作



左轮按压并旋转

可切换成双杯同时出咖啡。



左边是水箱

右边是咖啡豆

![image-20220116162456668](.assets/image-20220116162456668.png)



![img](https://cdn.shopify.com/s/files/1/0078/9502/3675/products/D6-Beverage-Ref_8e245d2d-35c0-49f8-a27e-72dac2dd1db2_1200x.progressive.jpg?567556)

Programming

按P键，进入programming

![image-20220116171217008](.assets/image-20220116171217008.png)

When we talk about coffee, strength is simply the **ratio of ground coffee to water**. If you don’t use enough water, your coffee will feel swampy in your mouth and the flavour will be extremely overpowering. If you use too much water, the coffee will feel exactly that, watery.

Coffee Strength：Extro Strong Mild Normal

Temperature：High Normal

完成设置后，左轮旋转至**Exit**，按压左轮确认即可。

# Jura Quick Tips & Fixes

## Common Drip Tray Error Messages

The Drip Tray catches excess water/coffee while the machine is operating. When the drip tray fills to its max capacity, the water reaches the bottom of the metal contacts, which rest on internal metal contacts that connect to the board, thus, creating an electrical circuit. The current travels from the drip tray contacts to the board. The machine signals to the user that the drip tray needs to be emptied.

| Message                              | Fix                                                          |
| :----------------------------------- | :----------------------------------------------------------- |
| Empty TrayTray MissingDrawer Missing | Before replacing any parts, try gently removing the metal contacts from the drip tray. Clean the contacts and the drip tray where the contacts are secured with soap and water. Accumulation of wet grounds in this vicinity will cause a false reading that the drip tray needs to be emptied. Also, clean the internal contacts with a cotton swab, slightly moistened with a mild cleaning agent for metal. Dry the internal contacts and contacts on Drip Tray before reinserting.If contacts are dulled or corroded, they need to be replaced. Also, check corrosion on internal contacts.Many models have a micro-switch that is activated by the Drip Tray when it is inserted, which signals to the board to proceed to the next step. If the micro-switch lever is broken, jammed or corroded, the machine will not recognize the Drip Tray is inserted. Replace the micro-switch, if necessary. |
| Empty Grounds                        | Many models have a micro-switch that is activated by the Drip Tray when it is inserted, which signals to the board to proceed to the next step. If the micro-switch lever is broken, jammed or corroded, the machine will not recognize the Drip Tray is inserted. Replace the micro-switch, if necessary.The Grounds Container Tray was reinserted too soon after the machine asked to empty the grounds. Take tray out and wait 10 seconds before reinserting. |

## Common Coffee Dispensing Errors

| Error                                                        | Fix                                                          |
| :----------------------------------------------------------- | :----------------------------------------------------------- |
| Coffee Dispensing SlowlyCoffee Dispensing UnevenlyCoffee Leaking Behind Spout | The Grinder is grinding the beans too fine. Adjust the grinder to a coarser setting. We recommend vacuuming out the Grinder before adjusting to a coarser grind. When beans are ground too finely, the grinds compact inside the grinder, causing an inconsistent grind. Only adjust the grinder coarseness setting while the grinder is activated. It may take 4-5 cups made on the same setting to determine if it is the optimum grind.If the spouts are dispensing coffee slowly or unevenly, then the coffee dispensing spouts are partially clogged. Running a cleaning and descaling cycle may solve this problem, but the spouts may need to be removed and manually cleaned.If no coffee is dispensed, or the coffee leaks behind the spouts, then the coffee dispensing spouts are clogged. The spouts need to be removed and manually cleaned. Coffee grinds accumulate in the dispensing spouts over time if the grind setting is too coarse, extremely oily beans are used or cleaning cycles have been neglected. |
| No Coffee Dispensed                                          | The Grinder is grinding the coffee beans too finely. This may cause a [System Fill](https://www.jura-parts.com/Jura-Fill-System-Message-s/262.htm) message to appear and the used grounds to be soaked and look like mud. Adjust the grinder to a coarser setting. Continuously grinding beans on an incorrect setting can cause an excess of back pressure, which may cause hoses to burst or the Water Pump to overwork until it fails.The spring and rod mechanism in the brew group drain valve is damaged, which causes the water for the brew cycle to drain into the drip tray without reaching the coffee grounds in the brew group. |

## Common Water Tank Errors

| Error                                                        | Fix                                                          |
| :----------------------------------------------------------- | :----------------------------------------------------------- |
| Water Leaking from Water TankFill Water Tank MessageWater Filter Not Tight | The Jura Water Tank contains an [O-Ring](https://www.jura-parts.com/Jura-Water-Tank-O-Ring-p/10115.htm) that is affixed to a plunger. The O-Ring allows the flow of water from the water tank when the spring to the plunger is compressed. If the Water Tank is leaking when it is not fitted onto the coffee machine, the Water Tank O-Ring may need to be replaced. If there is water leaking while the Water Tank is fitted onto the machine, the [Water Inlet Valve Gasket](https://www.jura-parts.com/Jura-Water-Inlet-Gasket-p/10154.htm) may need to be replaced, as well.If you are receiving a Fill Water Tank message when the Water Tank is filled with water and fitted onto the coffee machine, check the Magnetic Float inside the Water Tank. When the Water Tank is filled with water, the float rises and contacts the internal Water Level Sensor to notify the CPU the Water Tank is filled and to proceed to the next step. The float may get stuck due to sediment buildup, preventing it from making contact with the internal sensor, thus causing a Fill Water Message.If the Water Filter appears to be loose inside the Water Tank, inspect the Water Filter Bracket to ensure there are no broken pieces preventing it from tightly securing the Water Filter. Also, inspect the bottom of the Water Tank where the Water Filter is inserted to ensure there are no broken pieces. If the Water Filter is not secured properly, there is a chance air can enter the Water Circuit of the coffee machine, which may damage parts, particularly the Water Pump. |

## 咖啡分类

### 水系列

![image-20220116163730853](.assets/image-20220116163730853.png)

![image-20220116163744292](.assets/image-20220116163744292.png)

### 奶泡咖啡

![image-20220116163803470](.assets/image-20220116163803470.png)

Macchiato上撒点焦糖，就变成了焦糖玛奇朵(Caramel Macchiato)

![image-20220116163827501](.assets/image-20220116163827501.png)

在Latte上再加各种糖浆，就变成了XXLatte，例如抹茶拿铁。

![image-20220116163856709](.assets/image-20220116163856709.png)

![image-20220116163422678](.assets/image-20220116163422678.png)

![image-20220116163919406](.assets/image-20220116163919406.png)

### 鲜奶油系列

![image-20220116163957300](.assets/image-20220116163957300.png)

![image-20220116164010666](.assets/image-20220116164010666.png)

![image-20220116164030711](.assets/image-20220116164030711.png)

![image-20220116164046745](.assets/image-20220116164046745.png)

### 创意系列

![image-20220116164115475](.assets/image-20220116164115475.png)
